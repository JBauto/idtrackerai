Experimental setups
===================

Zebrafish (in the lab)
**********************
Here you have some photos of our juvenile zebrafish setup and a raw frame from the videos we collect with it
(same as Supplementary Figure 3 of the `paper <https://drive.google.com/open?id=1fYBcmH6PPlwy0AQcr4D0iS2Qd-r7xU9n>`_).

.. image:: /_static/fish_setup_LR.png
    :width: 800
**Zebrafish video setup**.
The main tank was placed inside a
box built with matte white acrylic walls (Supplementary Fig. 3a). The lighting was
based on infrared and RGB LED strips. A cylindrical retractable light diffuser
made of plastic ensured homogeneous illumination in the central part of the
main tank. A 20 MP monochrome camera (Emergent Vision HT-20000M) with
a 28-mm lens (ZEISS Distagon T* 28-mm f/2.0 Lens with ZF.2) was positioned
approximately 70 cm from the surface of the arena. To prevent reflections
of the room ceiling, we used black fabric to cover the top of the box
(Supplementary Fig. 3b). We used this setup to record zebrafish in groups
and in isolation. Videos of groups of 10, 60 and 100 fish were recorded in a
custom-made one-piece circular tank of 70-cm diameter, designed in-house.
The tank was filled with fish system water to a depth of 2.5 cm. The circular
tank was held in contact with the water of the main tank approximately 10 cm
above a white background to improve the contrast between animals and
background (Supplementary Fig. 3c). A water-recirculating system equipped
with a filter and a chiller ensured a constant water temperature of 28 °C.

In `this link <https://docs.google.com/spreadsheets/d/1Ot9kBn_gNbViecbzpKQxp55clfhrAO2bHIUT-H8Vxmg/edit?usp=sharing>`_
you can find the list of the different parts needed to build the experimental box, with its dimensions, and links to the
Thorlabs webpage.

A simple and cheap way to build a cylindrical light diffuser is to use two plastic gymnastic hoop and a
white translucent plastic shower curtain.
You can use plastic zip ties to join the curtains to the hoop.

The `Scientific Hardware Platform at the Champalimaud Foundation <https://www.cf-hw.org/>`_ has helped us to design
different parts of the setup.
In particular, the acrylic circular tank is a key piece of the setup.
As this tank is made of a single piece of acrylic it has not straight borders or corners.
We find it to be very useful to avoid biasing the movement of the fish towards corners, which is common in rectangular
tanks.

Here you have the different models of tanks that we have used in the lab:

*  `720mm circular tank model <https://drive.google.com/file/d/1HaV4zQuXPtOg1Ytl3S-TD94Wx5SO-Oiq/view?usp=sharing>`_
*  `500mm circular tank model <https://drive.google.com/file/d/17ltpNFrEs8Uya8ZoVdI1CEkxQe4kvzZZ/view?usp=sharing>`_

We always use a transparent tank on top of a white matte acrylic version of the same tank that acts background and helps to
increase the contrast between the animals and the background.
We separate the tanks around 2-5 cm from each other (vertically), and fill also the white tank with water.
This avoids reflections of the animals in the walls of the tank.

Zebrafish (in summer courses)
*****************************
For the `2018 QBIO Summer research course @KITP, UCSB <https://www.kitp.ucsb.edu/qbio/2018-course-description>`_ we
designed a portable setup that fitted protective flight case:

.. image:: /_static/qbio_KITP_ucsb_fish_setup.png
    :width: 800

Here you can find `the model <https://drive.google.com/file/d/1A03xqPhAgvf_JaEwE_k0dk9mVJ9owGE0/view?usp=sharing>`_,
`the dimensions of the aluminium rails structure <https://drive.google.com/file/d/14t8sZYDJGtiiNhgNcYu2zxoeg32lL6Tf/view?usp=sharing>`_,
`the 300mm circular tank model <https://drive.google.com/file/d/194aQ7kSj_1-dxdHAYkZAO6oHifPPkQOM/view?usp=sharing>`_,
and `the tank holder and light diffuser frame models <https://drive.google.com/file/d/1FqDSqmR1O4TYBdPS15AgC4FjIfS4v9nK/view?usp=sharing>`_,
